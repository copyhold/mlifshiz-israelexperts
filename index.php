<?php
if ($_POST) {
  $message = strtr("
    שם: name
    Email: email
    טלפון: tel
    ",$_POST);
  $to = 'Joyce@IsraelExperts.com, israelexperts.signup@gmail.com'; 
  mb_internal_encoding("UTF-8");
  mb_send_mail($to, 'landing page form', $message, "Mime-Version: 1.0\nContent-type: text/plain;charset=utf-8 \r\nFrom: noreply@israelexperts.com\r\nReply-To: " . $_POST['email'] . "\r\n");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">

  <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <title>Israel Experts</title>
<link rel="stylesheet" href="style.css">
</head>
<body class="<?php if ($_GET['thankyou']) echo 'thankyou'; ?>">
<header>
  <a class="logo" href="/"></a>
  <a class=gap href="#"></a>
  <a class="so fb" href=""></a>
  <a class="so in" href=""></a>
  <a class="so tw" href=""></a>
</header>  
<section class="form">
  <h1>Planning a family trip to Israel?</h1>
  <h4>Let's create an adventure together</h4>
  <form action="index.php?thankyou=1" method=post>
    <h3>CONTACT US FOR A QUOTE</h3>
    <fieldset>
      <input required placeholder="Full Name" type="text" name="name">
      <input required placeholder="Email" type="email" name="email">
      <input required placeholder="Phone" type="tel" name="tel">
      <button type="submit">FIND MY ISRAEL TRIP</button>
    </fieldset>
  </form>
  <h5>Yes, I want to tame my family to Israel</h5>
  <p>Questions?</p>
  <a href="tel:2127964111">CALL 212.796.4111</a>
  <div class="thankyou">
    <h1>THANK YOU!</h1>
    <div>We now have your email and phone. Be prepared</div>
  </div>
</section>
<article>
  <p>We know Israel. We love Israel. And we want to get to know you, so you too can fall in love with Israel. Israel Experts is an Israel-based tour operator that specializes in creating unique family vacations filled with meaningful experiences and memorable moments for the entire family. </p>
  <p>It's YOUR Israel experience, so whatever your family enjoys - we'll make it happen. </p>
  <p>Our professionals know every rock, path, beach and market stall. After getting to know your family we'll weave together an experience that everyone will enjoy. We'll take you to sites on and off the beaten track, get you to the best falafel, tell you tales of wonder and teach you how to find the tastiest Knafe*. When you need a little R&R we'll recommend the perfect beach to relax on, a mall to shop at, and a great restaurant to celebrate in. </p>
  <p>*Knafe - an Arabic desert that’s white, bright orange, served warm, and the best thing you'll ever eat!!) </p>
  <h5>Yes, I want to tame my family to Israel</h5>
  <p>Questions?</p>
  <a href="tel:2127964111">CALL 212.796.4111</a>
</article>
<main>
  <h2>WHAT’S INCLUDED</h2>
  <dl>
    <dt>Fully personalized tour</dt>
    <dd>We’ll ask all the right questions to get you to all the right places.</dd>
    <dt>Rest & Relaxation </dt>
    <dd>At the end of a fun day’s travel we’ll make sure you have a place to lay your hat.</dd>
    <dt>Travel in Style </dt>
    <dd>How you get there is as important as where you’re going. We’ll book the right transportation for you.</dd>
    <dt>Hassel Free Travel </dt>
    <dd>We think of every detail so you can be sure all attractions and entry fees are included, booked and pre-paid.</dd>
  </dl>
  <p>And above all, we are here; from the moment you land to the time you depart –<br>for anything your family may need.</p>
  <h3>Let’s start planning together</h3>
</main>
<iframe class=video src="https://www.youtube.com/embed/de7pCdWIgbQ" frameborder="0" allowfullscreen></iframe>
<section class="test">
  <h2>OUR CLIENTS SAY</h2>
<blockquote>
  <p>"On behalf of my entire family, we wanted to thank you and and everyone who was involved in helping to plan and organize our trip. We had a "first class experience" from the time we arrived at the Ben Gurion Airport and a representative was waiting for us to take us to our hotel in Tel Aviv, to then meeting the wonderful Mr. Moshe Gissis (best guide in the whole world).</p>
  <p>We traveled to so many beautiful places and each and every place you recommended and all of our accommodations were just outstanding!!!!!! I could never have planned a trip like this on my own. Also, we never had to worry about anything. Moshe took good care of our whole family and everything went so smoothly that I can't believe it. It was an amazing trip and definitely a “once in a lifetime" experience.</p>
  <p>My children are asking, "When can we go back"? Again, we want to thank all of you from the bottom of our hearts for everything you did for us and for providing the best trip of our lives. Warm wishes."</p>
  <cite> Micki Cohen and the whole Cohen/Broz family </cite>
</blockquote>
<hr>
<blockquote>
  <p>"We have returned from our trip to Israel, and I wanted to thank you for organizing a most wonderful and professional stay. Everything and everyone were there as planned, and the whole vacation went off without a hitch...</p>
  <p>You run an exact and professional operation and not only will we use you again for our next trip to Israel, but will be recommending you to anyone traveling to the area. Toda Raba!"</p>
  <cite> Carlos Meyer Rish, MD and Nayla Bitar, Charlotte NC </cite>
</blockquote>
</section>
<footer>
  <h2>When you think Israel - think IsraelExperts!  </h2>
  <p>We strive to open people’s minds & hearts to the inspiring and complex story of the country we love.</p>
  <p>We create experiences that encourage travelers to engage in the beauty and multi-dimensional mosaic that is Israel.</p>
  <p>We create exceptional, tailor made, fun, educational tours through Israel. We’d love to do that for you.</p>
  <h3> Come to Israel with the Experts  </h3>
  <a href="tel:2127964777">CALL US - 212.796.4777<br>OR, WE'LL CALL YOU: </a>
</footer>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-13204786-3', 'auto');
ga('send', 'pageview');

</script>
<?php if ($_GET['thankyou']) : ?>
<script>setTimeout(function() { location.href = "http://www.israelexperts.com/" }, 4000); </script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1025640144;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "xB6FCNHt-1wQ0I2I6QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1025640144/?label=xB6FCNHt-1wQ0I2I6QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif ?>
</body>
</html>
